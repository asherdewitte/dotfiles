" Turn off filetype detection during install
filetype off
set nocompatible

" Initialise the pathogen vim plugin handler
set rtp+=~/.vim/bundle/vundle
call vundle#begin()
Plugin 'VundleVim/Vundle.vim', {'name': 'vundle'}

" Setup plugins
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'othree/yajs.vim'
Plugin 'stephpy/vim-yaml'
Plugin 'mkitt/tabline.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'fatih/vim-go'
Plugin 'kien/ctrlp.vim'
Plugin 'othree/es.next.syntax.vim'
Plugin 'rhysd/vim-crystal'

" End Setup
call vundle#end()
filetype plugin indent on

" Turn on syntax highlighting
syntax on

" Set netrw (the inbuilt file browser) with some nicer settings
let g:netrw_liststyle=3
let g:netrw_list_hide= '.*\.sw[a-z]$,.*\.pyc$,__pycache__\/'
let g:netrw_banner=0

" Turn on line numbering (absolute + relative hybrid)
set number rnu

" Setup nice defaults for tabbing and indentation
set backspace=indent,eol,start tabstop=4 shiftwidth=4 nocindent expandtab smartindent

" Turn on the statusbar
set laststatus=2

" Fix the tmux issue with colors
set t_Co=256

" Map space the "leader" keyboard shortcut
let mapleader=" "
map <leader>k :E<cr>

" No Bad Habits Matey (disable arrows)
noremap  <Up> ""
noremap! <Up> <Esc>
noremap  <Down> ""
noremap! <Down> <Esc>
noremap  <Left> ""
noremap! <Left> <Esc>
noremap  <Right> ""
noremap! <Right> <Esc>
inoremap jk <esc>

" Code folding
set fdm=indent

" Don't store swap files in the directory
set directory=$HOME/.vim/swapfiles//

" Set javascript specific niceness for indentation
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2 expandtab foldmethod=syntax
