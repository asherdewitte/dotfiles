# Enable antigen
source $HOME/.zsh/antigen/antigen.zsh

# Use a directory in home for the function path
fpath=( '$HOME/.zsh/functions' $fpath )
setopt prompt_subst

# Allow skip words using ctrl left + right
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# Enable antigen functions
antigen bundle git
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting

# Enable the pure theme
antigen bundle mafredri/zsh-async
antigen bundle sindresorhus/pure

if which crystal > /dev/null; then
    # Enable crystal tools
    antigen bundle veelenga/crystal-zsh
fi

# Tell antigen we've stopped config
antigen apply

# Setup golang paths
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# Add some awesome aliases
alias dotfiles="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias dotpackage-add="dotfiles submodule add"
alias dotpackage-remove="dotfiles submodule deinit"
alias dotpackage-list="dotfiles submodule status"
alias weather="wego"
alias shellconf="vim $HOME/.zshrc"
alias phplint="find . -name \"*.php\" -print0 | xargs -0 -n1 -P8 php -l > /dev/null"
alias ugh='sudo $(fc -ln -1)'
alias crap='sudo $(fc -ln -1)'
alias gcl='git branch --merged | grep -v \* | xargs git branch -D'
alias gcr='git branch -a --merged | grep -v \* | xargs git branch -D'

function pomodoro () { termdown -bc 10 -t "Take a 5 minute break\!" "25m" }

# Nicely include secrets
if [ -d $HOME/.zsh/secrets ] && [ "$(ls -A $HOME/.zsh/secrets)" ]; then
	for file in $HOME/.zsh/secrets/*.secret; do
		. $file
	done
fi

if [ -d $HOME/.pyenv/bin ]; then
    export PATH="$HOME/.pyenv/bin:$PATH"
    if [ ! -d $HOME/.pyenv/plugins/pyenv-virtualenv ]; then
        git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
    fi
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

if [ -d $HOME/.nodenv/bin ]; then
    export PATH="$PATH:$HOME/.nodenv/bin"
    eval "$(nodenv init -)"
fi

if [ -d $HOME/.linkerd2/bin ]; then
    export PATH="$PATH:$HOME/.linkerd2/bin"
fi
if [ -f $HOME/.cargo/env ]; then
    source $HOME/.cargo/env
fi

# Allow bash style comments in cli
setopt interactivecomments
if [ -d $HOME/.local/bin ]; then
    export PATH=$PATH:$HOME/.local/bin
fi


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -d "$HOME/.nodenv/bin" ]; then
    export PATH="$HOME/.nodenv/bin:$PATH"
    eval "$(nodenv init -)"
fi

if which brew > /dev/null; then
    source $(brew --prefix php-version)/php-version.sh && php-version 7
fi

if which yarn > /dev/null; then
    export PATH="$(yarn global bin):$PATH"
fi


if [ -d "$HOME/.krew/bin" ]; then
    export PATH="${PATH}:${HOME}/.krew/bin"
fi


RPROMPT="%D{%y/%m/%f} | %@ %D{%Z}"
export TZ="/usr/share/zoneinfo/Australia/Brisbane"
